
;; Simple github module:
;;
;; Set github-user and github-api-token to appropriate values if you want
;; authenticated access.
;;
;; Call (github-get arg...) to do a GET request with the given
;; arguments (concatenated to the end of the URL),
;; eg.  (github-get "user/show/robtillotson") to see my info.
;;

;; Copyright 2010 Rob Tillotson <rob@pyrite.org>
;;
;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
;; THE SOFTWARE.

(module github
  (github-authenticate github-unauthenticate github-get)

  (import scheme chicken extras)
  (use intarweb http-client uri-common json srfi-1 srfi-13 ports posix)

  (define github-user #f)
  (define github-password #f)
  (define github-use-token #f)
  
  (define github-api-url "https://github.com/api/v2/json/")

  ;; Authenticate to github
  (define (github-authenticate #!optional user password use-token)
    (if github-user
        (begin
          (set! github-user user)
          (set! github-password password)
          (set! github-use-token use-token))
        (begin
          (set! github-user (git-config-get "github.user"))
          (set! github-password (git-config-get "github.token"))
          (set! github-use-token #t))))

  (define (github-unauthenticate)
    (set! github-user #f)
    (set! github-password #f))

  ;; Execute an external command and return a string of the results.
  (define (do-shell-command cmd)
    (call-with-input-pipe cmd (lambda (port) (read-string #f port))))

  (define (git-config-get var)
    (string-trim-both (do-shell-command (string-append "git config --get " var))))

  (define (github-make-request apiurl host port . path)
    (let ((request (make-request uri: (uri-reference (string-append apiurl (string-join path "/")))
                                 port: port
                                 headers: (headers `((host (,host . 443)))))))
      (if github-user
          (let ((user (if github-use-token (string-append github-user "/token") github-user)))
            (update-request request headers: (headers `((authorization #(basic ((username . ,user)
                                                                                (password . ,github-password)) ))))))
          request)
      ))

  (define (github-api-make-request . path)
    (apply github-make-request github-api-url "github.com" 443 path))
  
  ;; Parse a github json response, including transforming
  ;; json structs (returned from the json egg as vectors) into alists,
  ;; and stripping the outer alist if the response contains only a
  ;; single object.
  (define (github-parse-response-json port)
    (let ((response (json-structs-to-alists (json-read port))))
      (if (equal? (length response) 1)
          (cdr (car response))
          response)))

  (define (json-structs-to-alists lst)
    (cond ((vector? lst)
           (map (lambda (pair) (cons (car pair) (json-structs-to-alists (cdr pair)))) (vector->list lst)))
          ((list? lst)
           (map json-structs-to-alists lst))
          (else lst)))

  (define (github-perform-get-request req)
    (call-with-input-request req #f github-parse-response-json))

  ;; All purpose github GET requester.  Provide the rest of the URL parts as arguments
  ;; and you'll get back whatever JSON the request returns.
  (define (github-get . lst)
    (github-perform-get-request (apply github-api-make-request lst)))

  ;; ***** Users
  (export github-user-info github-my-info github-user-search github-user-following
          github-user-followers github-user-watching github-my-keys github-my-emails)
  
  (define (github-user-info name) (github-get "user/show" name))
  (define (github-my-info) (github-get "user/show"))
  (define (github-user-search criterion) (github-get "user/search" criterion))
  (define (github-user-following name) (github-get "user/show" name "following"))
  (define (github-user-followers name) (github-get "user/show" name "followers"))
  (define (github-user-watching name) (github-get "repos/watched" name))
  (define (github-my-keys) (github-get "user/keys"))
  (define (github-my-emails) (github-get "user/emails"))

  ;; ***** Organizations and Teams
  (export github-org-info github-user-orgs github-my-orgs github-my-org-repositories
          github-org-repositories github-org-members github-org-teams github-team-info
          github-team-members github-team-repositories)
  
  (define (github-org-info name) (github-get "organizations" name))
  (define (github-user-orgs user) (github-get "user/show" user "organizations"))
  (define (github-my-orgs) (github-get "organizations"))
  (define (github-my-org-repositories) (github-get "organizations/repositories"))
  (define (github-org-repositories org) (github-get "organizations" org "public_repositories"))
  (define (github-org-members org) (github-get "organizations" org "public_members"))
  (define (github-org-teams org) (github-get "organizations" org "teams"))
  (define (github-team-info teamid) (github-get "teams" (number->string teamid)))
  (define (github-team-members teamid) (github-get "teams" (number->string teamid) "members"))
  (define (github-team-repositories teamid) (github-get "teams" (number->string teamid) "repositories"))

  ;; ***** Issues
  (export github-issues github-issue-search github-issues-with-label github-issue
          github-issue-comments github-issue-labels)
  
  (define (github-issues user repo #!optional (state 'open))
    (github-get "issues/list" user repo (symbol->string state)))
  (define (github-issue-search user repo term #!optional (state 'open))
    (github-get "issues/search" user repo (symbol->string state) term))
  (define (github-issues-with-label user repo label)
    (github-get "issues/list" user repo "label" label))
  (define (github-issue user repo issueid)
    (github-get "issues/show" user repo (number->string issueid)))
  (define (github-issue-comments user repo issueid)
    (github-get "issues/comments" user repo (number->string issueid)))
  (define (github-issue-labels user repo)
    (github-get "issues/labels" user repo))

  ;; ***** Repositories
  (export github-search-repositories github-repository-info github-user-repositories
          github-watch-repository github-unwatch-repository github-fork-repository
          github-repository-keys github-repository-collaborators github-pushable-repositories
          github-repository-teams github-repository-contributors github-repository-watchers
          github-repository-network github-repository-languages github-repository-tags
          github-repository-branches)
  
  (define (github-search-repositories term) (github-get "repos/search" term))
  (define (github-repository-info user repo) (github-get "repos/show" user repo))
  (define (github-user-repositories user) (github-get "repos/show" user))
  ;; watch, unwatch, and fork are get requests?  hmm
  (define (github-watch-repository user repo) (github-get "repos/watch" user repo))
  (define (github-unwatch-repository user repo) (github-get "repos/unwatch" user repo))
  (define (github-fork-repository user repo) (github-get "repos/fork" user repo))
  (define (github-repository-keys user repo) (github-get "repos/keys" user repo))
  (define (github-repository-collaborators user repo) (github-get "repos/show" user repo "collaborators"))
  (define (github-pushable-repositories) (github-get "repos/pushable"))
  (define (github-repository-teams user repo) (github-get "repos/show" user repo "teams"))
  (define (github-repository-contributors user repo) (github-get "repos/show" user repo "contributors"))
  (define (github-repository-watchers user repo) (github-get "repos/show" user repo "watchers"))
  (define (github-repository-network user repo) (github-get "repos/show" user repo "network"))
  (define (github-repository-languages user repo) (github-get "repos/show" user repo "languages"))
  (define (github-repository-tags user repo) (github-get "repos/show" user repo "tags"))
  (define (github-repository-branches user repo) (github-get "repos/show" user repo "branches"))

  ;; ***** Commits
  (export github-commits github-commits-for-file github-commit-info)

  (define (github-commits user repo branch) (github-get "commits/list" user repo branch))
  (define (github-commits-for-file user repo branch path) (github-get "commits/list" user repo branch path))
  (define (github-commit-info user repo sha) (github-get "commits/show" user repo sha))

  ;; ***** Objects
  (export github-tree github-blob github-blob-info github-list-blobs github-list-blob-info
          github-list-tree-info github-blob-raw)
  
  (define (github-tree user repo sha) (github-get "tree/show" user repo sha))
  (define (github-blob user repo treesha path) (github-get "blob/show" user repo treesha path))
  (define (github-blob-info user repo treesha path)
    (github-get "blob/show" user repo treesha (string-append path "?meta=1")))
  (define (github-list-blobs user repo treesha) (github-get "blob/all" user repo treesha))
  (define (github-list-blob-info user repo treesha) (github-get "blob/full" user repo treesha))
  (define (github-list-tree-info user repo treesha) (github-get "tree/full" user repo treesha))
  (define (github-blob-raw user repo blobsha)
    (call-with-input-request (github-api-make-request "blob/show" user repo blobsha)
                             #f
                             (lambda (port) (read-string #f port))))

  ;; ***** Networks
  ;; Not implemented yet
  
  ;;
  ;; Gist support, which isn't quite the same API as the rest of github
  ;;
  (export gist-get gist-get-file gist-get-gists)
  
  (define gist-api-url "https://gist.github.com/api/v1/json/")

  (define (gist-api-make-request . path)
    (apply github-make-request gist-api-url "gist.github.com" 443 path))

  ;; Get a gist.  Note that the API returns not only a structure with only a single
  ;; element, but its content is a single element list, so strip that too.
  (define (gist-get gistid)
    (car (github-perform-get-request (gist-api-make-request (number->string gistid)))))

  (define (gist-get-file gistid filename)
    (call-with-input-request (github-make-request "https://gist.github.com/raw/" "gist.github.com" 443 (number->string gistid) filename)
                             #f
                             (lambda (port) (read-string #f port))))

  (define (gist-get-gists user)
    (github-perform-get-request (gist-api-make-request "gists" user))))
